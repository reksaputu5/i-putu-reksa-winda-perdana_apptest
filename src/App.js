import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AddContact from "./components/add-contact.component";
import Tutorial from "./components/contact.component";
import ContactList from "./components/contact-list.component";

class App extends Component {
  render() {
    return (
      <div style={{backgroundColor: "aliceblue", height: '100%'}}>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/index"} className="navbar-brand">
            AppTest I PUTU REKSA W.P
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/index"} className="nav-link">
                Index
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/index"]} component={ContactList} />
            <Route exact path="/add" component={AddContact} />
            <Route path="/contact/:id" component={Tutorial} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
