import React, { Component } from "react";
import TutorialDataService from "../services/contact.service";
import { Link } from "react-router-dom";

export default class ContactList extends Component {
  constructor(props) {
    super(props);
    this.retrieveContact = this.retrieveContact.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveContact = this.setActiveContact.bind(this);

    this.state = {
      contacts: [],
      currentContact: null,
      currentIndex: -1,
      searchTitle: ""
    };
  }

  componentDidMount() {
    this.retrieveContact();
  }


  retrieveContact() {
    TutorialDataService.getAll()
      .then(response => {
        console.log(response.data.data);
        this.setState({
          contacts: response.data.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveContact();
    this.setState({
      currentContact: null,
      currentIndex: -1
    });
  }

  setActiveContact(contact, index) {
    this.setState({
      currentContact: contact,
      currentIndex: index
    });
  }


  render() {
    const {  contacts, currentContact, currentIndex } = this.state;

    return (
      <div className="list row">
        
        <div className="col-md-6" style={{cursor:'pointer'}}>
          <h4>Contact List</h4>

          <ul className="list-group">
            {contacts.map((rowContact, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveContact(rowContact, index)}
                  key={index}
                >
                  {rowContact.firstName}
                </li>
              ))}
          </ul>

        </div>
        <div className="col-md-6">
          {currentContact ? (
            <div>
              <h4>Contact Detil</h4>
              <div>
                <label>
                  <strong>First Name:</strong>
                </label>{" "}
                {currentContact.firstName}
              </div>
              <div>
                <label>
                  <strong>Last Name:</strong>
                </label>{" "}
                {currentContact.lastName}
              </div>
              <div>
                <label>
                  <strong>AGE:</strong>
                </label>{" "}
                {currentContact.age}
              </div>

              <Link
                to={"/contact/" + currentContact.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              
            </div>
          )}
        </div>
      </div>
    );
  }
}
