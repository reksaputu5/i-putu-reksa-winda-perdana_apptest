import React, { Component } from "react";
import TutorialDataService from "../services/contact.service";

export default class Tutorial extends Component {
  constructor(props) {
    super(props);
    this.onChangefirstName = this.onChangefirstName.bind(this);
    this.onChangelastName = this.onChangelastName.bind(this);
    this.onChangeAge = this.onChangeAge.bind(this);
    this.getTutorial = this.getTutorial.bind(this);
    this.updateTutorial = this.updateTutorial.bind(this);
    this.deleteTutorial = this.deleteTutorial.bind(this);

    this.state = {
      currentTutorial: {
        firstName: "",
        lastName: "", 
        age: 0,
        photo: "http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550",
        published: false
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getTutorial(this.props.match.params.id);
  }

  onChangefirstName(e) {
    const firstName = e.target.value;

    this.setState(function(prevState) {
      return {
        currentTutorial: {
          ...prevState.currentTutorial,
          firstName: firstName
        }
      };
    });
  }

  onChangelastName(e) {
    const lastName = e.target.value;
    
    this.setState(prevState => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        lastName: lastName
      }
    }));
  }

  onChangeAge(e) {
    const age = e.target.value;
    
    this.setState(prevState => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        age: age
      }
    }));
  }

  getTutorial(id) {
    TutorialDataService.get(id)
      .then(response => {
        console.log(response.data.data);
        this.setState({
          currentTutorial: response.data.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  
  updateTutorial() {
    var data = {
      firstName: this.state.currentTutorial.firstName,
      lastName: this.state.currentTutorial.lastName,
      age: this.state.currentTutorial.age,
      photo: this.state.currentTutorial.photo
    };
    TutorialDataService.update(
      this.state.currentTutorial.id,
      data
    )
      .then(response => {
        console.log(response);
        this.setState({
          message: response.data.message
        });
      })
      .catch(e => {
        console.log(e.response.data);
        this.setState({message: e.response.data.message});
      });
  }

  deleteTutorial() {    
    TutorialDataService.delete(this.state.currentTutorial.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/contact')
      })
      .catch(e => {
        console.log(e);
        this.setState({message: e.response.data.message});
      });
  }

  render() {
    const { currentTutorial } = this.state;

    return (
      <div>
        {currentTutorial ? (
          <div className="edit-form">
            <h4>Edit Contact Detil</h4>
            <form>
            <div className="form-group">
              <label htmlFor="firstName">First Name</label>
              <input
                type="text"
                className="form-control"
                id="firstName"
                required
                value={currentTutorial.firstName}
                onChange={this.onChangefirstName}
                name="firstName"
              />
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Last Name</label>
              <input
                type="text"
                className="form-control"
                id="lastName"
                required
                value={currentTutorial.lastName}
                onChange={this.onChangelastName}
                name="lastName"
              />
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Age</label>
              <input
                type="number"
                className="form-control"
                id="age"
                required
                value={currentTutorial.age}
                onChange={this.onChangeAge}
                name="age"
              />
            </div>
            </form>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updateTutorial}
            >
              Save Update
            </button>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteTutorial}
            >
              Delete
            </button>

            
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            
          </div>
        )}
      </div>
    );
  }
}
