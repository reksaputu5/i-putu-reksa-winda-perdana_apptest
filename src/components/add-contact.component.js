import React, { Component } from "react";
import ContactDataService from "../services/contact.service";

export default class AddContact extends Component {
  constructor(props) {
    super(props);
    this.onChangefirstName = this.onChangefirstName.bind(this);
    this.onChangelastName = this.onChangelastName.bind(this);
    this.onChangeAge = this.onChangeAge.bind(this);
    this.saveContact = this.saveContact.bind(this);
    this.newContact = this.newContact.bind(this);

    this.state = {
      firstName: "",
      lastName: "", 
      age: 0,
      photo: "http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550",
      submitted: false,
      message: ""
    };
  }

  onChangefirstName(e) {
    this.setState({
      firstName: e.target.value
    });
  }

  onChangelastName(e) {
    this.setState({
      lastName: e.target.value
    });
  }

  onChangeAge(e) {
    this.setState({
      age: e.target.value
    });
  }

  saveContact() {
    var data = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      age: this.state.age,
      photo: this.state.photo
    };

    ContactDataService.create(data)
      .then(response => {
        console.log(response.data.data);
        this.setState({
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          age: response.data.age,
          photo: response.data.photo,
          submitted: true
        });
      })
      .catch(e => {
        // console.log(e);
        this.setState({message: e.response.data.message});
      });
  }

  newContact() {
    this.setState({
      firstName: "",
      lastName: "", 
      age: 0,
      photo: "",
      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newContact}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="firstName">First Name</label>
              <input
                type="text"
                className="form-control"
                id="firstName"
                required
                value={this.state.firstName}
                onChange={this.onChangefirstName}
                name="firstName"
              />
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Last Name</label>
              <input
                type="text"
                className="form-control"
                id="lastName"
                required
                value={this.state.lastName}
                onChange={this.onChangelastName}
                name="lastName"
              />
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Age</label>
              <input
                type="number"
                className="form-control"
                id="age"
                required
                value={this.state.age}
                onChange={this.onChangeAge}
                name="age"
              />
            </div>

            <button onClick={this.saveContact} className="btn btn-success">
              Submit
            </button>
            <p>{this.state.message}</p>
          </div>
        )}
      </div>
    );
  }
}
